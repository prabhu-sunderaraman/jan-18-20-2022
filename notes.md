* Enterprise applications
* Security, Transaction, Object pooling, Messaging

``` java
class Account {
	accountNumber, balance
	deposit(amount) {
	}
	withdraw(amount) {
	}
	transferFund(from, to) {
	}
}
```
* Pool of Account objects and have them ready in the memory
* When the customer logs out, you move the Account object back to the pool
* transferFund to have __transaction__ functionality
* Security for withdraw and transferFund

### Enterprise Java Beans (EJB)

* Heavy weight Container

``` java
interface AccountHome extends Home {}
interface Account extends Remote {
	deposit, withdraw, transferFund
}
class AccountBean implements EntityBean {
	//implement deposit, withdraw, transferFund
	//No constructor
	//No static members
	//Not throw Exceptions
	//No final members
	//Not extend any other class
}
ejb-jar.xml 

//zip all these 4 items into a jar file and then drop them into the container

```


### Spring framework

* Lightweight container
* POJO framework
* collection of jars
* Add them to your classpath
* Write Java Beans
* Configure the services you need using xml files or annotations
* Container loads these and provides the neccessary functionality
* __Dependency Injection__: It's a simple mechanism by which you avoid unneccessary creation and lookup code.
* __ApplicationContext__ which acts as a container

* Version 5.x
* spring.io

* Cloud started getting popular in the late 2000s
* Infrastructure was moved to the cloud slowly
* Abstraction of spring framework is __Spring Boot__
* 2.x -> Spring 5.x
* Web applications, Command line applications, Cron Jobs, Messaging applications


































